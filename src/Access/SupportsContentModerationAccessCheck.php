<?php

declare(strict_types=1);

namespace Drupal\scheduled_transitions\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\scheduled_transitions\Form\ScheduledTransitionsSettingsForm as SettingsForm;
use Drupal\scheduled_transitions\Routing\ScheduledTransitionsRouteProvider;
use Drupal\scheduled_transitions\ScheduledTransitionsUtilityInterface;
use Symfony\Component\Routing\Route;

/**
 * Check if entity supports content moderation.
 *
 * Supports revisions, has active workflow, etc.
 */
class SupportsContentModerationAccessCheck implements AccessInterface {

  /**
   * Value of 'applies_to' in service tag.
   */
  public const ACCESS_CHECK_ID = '_scheduled_transitions_supports_content_moderation';

  public function __construct(
    protected ModerationInformationInterface $moderationInformation,
    protected ScheduledTransitionsUtilityInterface $scheduledTransitionsUtility,
  ) {
  }

  /**
   * Checks the entity supports content moderation.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    /** @var string $routeEntityTypeId */
    $routeEntityTypeId = $route
      ->getOption(ScheduledTransitionsRouteProvider::ROUTE_ENTITY_TYPE);

    $entity = $route_match->getParameter($routeEntityTypeId);
    if ($entity instanceof ContentEntityInterface) {
      $access = (new CacheableMetadata())
        ->addCacheableDependency($entity)
        ->addCacheTags([SettingsForm::SETTINGS_TAG]);
      $enabledBundles = $this->scheduledTransitionsUtility->getBundles();
      if (\in_array($entity->bundle(), $enabledBundles[$entity->getEntityTypeId()] ?? [], TRUE) && $this->moderationInformation->isModeratedEntity($entity)) {
        return AccessResult::allowed()->addCacheableDependency($access);
      }
      return AccessResult::forbidden('Scheduled transitions not supported on this entity.')
        ->addCacheableDependency($access);
    }
    return AccessResult::forbidden('No entity provided.');
  }

}
