<?php

/**
 * @file
 * Post update functions.
 */

declare(strict_types=1);

/**
 * Reset service container.
 */
function scheduled_transitions_post_update_202406_container_reset(?array &$sandbox = NULL): void {
  // No-op.
}
